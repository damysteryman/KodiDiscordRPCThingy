using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KodiDiscordRPCThingy
{
    public static class UI
    {
        public static StringList Banner()
        {
            string str = $"Ver: {((AssemblyInformationalVersionAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyInformationalVersionAttribute)))).InformationalVersion}";
            string author = $"By {((AssemblyCompanyAttribute)(Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyCompanyAttribute)))).Company}";
            StringList banner = new StringList { "KodiDiscordRPCThingy", str, author };
            banner.PutInBorder();
            return banner;
        }

        public static StringList PutInBorder(this string _str, int _width = -1, bool _startBorder = true, bool _endBorder = true)
        {
            if (_width < 0)
                _width = _str.Length + 4;
                
            StringList strs = new StringList { _str };
            strs.PutInBorder(_width, _startBorder, _endBorder);

            return strs;
        }

        public static StringList Usage()
        {
            StringList usage = new StringList
            { 
                "Usage: kodidiscordrpcthingy --app-id <id of discord bot app> --host <hostname> --port <host port> --auth <discord auth token file>", ""
            };
            usage.Add("Required arguments:");
            usage.Add("        --app-id <arg>          App ID of Discord app/bot you are using for Rich Presence.");
            usage.Add("");
            usage.Add("Optional arguments:");
            usage.Add("        --host <arg>            Hostname of Kodi Remote Control webserver.");
            usage.Add("                                Default is 'localhost'.");
            usage.Add("        --port <arg>            Host Port of Kodi Remote Control webserver.");
            usage.Add("                                Default is '8081'.");
            usage.Add("        --art-path <arg>        Path th dir where art asset files should be placed during/after preparation.");
            usage.Add("                                Default is your OS's temp files folder");
            usage.Add("        --auth <arg>            Path to file containing a Discord User Token that is authorized to upload art assets");
            usage.Add("                                to the Discord app specified with --app-id. ");
            usage.Add("                                Necessary to auto-upload poster of the currently playing item to Discord.");
            usage.Add("");
            usage.Add("              WARNING! Using --auth argument and auto-uploading poster art assets to discord");
            usage.Add("                       might be considered a form of self-botting, which is against Discord's ToS.");
            usage.Add("                       Use this feature at your own risk.");
            usage.Add("");
            usage.Add("        --help                  Display this message and exit program.");

            return usage;
        }
    }
}