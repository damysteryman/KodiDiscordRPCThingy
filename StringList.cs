using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KodiDiscordRPCThingy
{
    public class StringList : List<string>
    {
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string str in this)
                sb.Append(str + Environment.NewLine);

            return sb.ToString();
        }

        public void PutInBorder(int _width = -1, bool _startBorder = true, bool _endborder = true)
        {
            if (_width < 0)
            {
                _width = this.Max(val => val.Length) + 4;
            }

            string border = "=";
            while (border.Length < _width)
                border += "=";

            for (int i = 0; i < this.Count; i++)
            {
                int len = _width - 1;
                if (!this[i].StartsWith("--"))
                    this[i] = " " + this[i];
                this[i] = "=" + this[i];
                while (this[i].Length < len)
                    if (this[i].EndsWith("--"))
                        this[i] += "-";
                    else
                        this[i] += " ";
                if (this[i].Length == len)
                    this[i] += "=";
            }
            if (_startBorder) this.Insert(0, border);
            if (_endborder) this.Add(border);
        }
    }
}
