﻿
using DiscordRPC;
using DiscordRPC.Logging;
using DiscordRPC.Message;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace KodiDiscordRPCThingy
{
    class Asset
    {
        public int type { get; set; }
        public string id { get; set; }
        public string name { get; set; }

        public Asset(int _type, string _id, string _name)
        {
            type = _type;
            id = _id;
            name = _name;
        }

        public Asset()
        {
            type = 1;
            id = "";
            name = "";
        }
    }

    class Program
	{
        private const string APP_URL_BASE = "https://discord.com/api/oauth2/applications/";
        private static string APP_ASSET_URL = "";
        private static string APP_ID = "";
        private static string KODI_HOST = "localhost";
        private static string KODI_PORT = "8081";
        private static string AUTH_TOKEN = "";
        private static string ASSET_PATH = Path.GetTempPath();
        private static string posterOld = "";
        private static LogLevel logLevel = LogLevel.Info;
        private static DiscordRpcClient client;
        private static bool isRunning = true;
        private static Thread Background = new Thread(new ThreadStart(UpdatePresenceLoop));
        private static RichPresence oldPresence;

        //Main Loop
        static void Main(string[] args)
        {
            Console.WriteLine(UI.Banner());
            // Read program args and setup vars
            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "--help":
                        Console.WriteLine(UI.Usage());
                        return;

                    case "--auth":
                        string filePath = args[++i];
                        try
                        {
                            AUTH_TOKEN = File.ReadAllText(filePath);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("EXCEPTION: Unable to load specified token file.");
                            Console.WriteLine("Specified file: " + filePath);
                            Console.WriteLine("Exception Details:" + Environment.NewLine + e.Message + Environment.NewLine + e.StackTrace);
                        }
                        break;

                    case "--app-id":
                        APP_ID = args[++i];
                        break;

                    case "--host":
                        KODI_HOST = args[++i];
                        break;

                    case "--port":
                        KODI_PORT = args[++i];
                        break;

                    case "--art-path":
                        ASSET_PATH = args[++i];
                        break;

                    default:
                        Console.WriteLine($"WARNING: argument '{args[i]}' not valid, ignoring...");
                        break;
                }
            }

            if (string.IsNullOrEmpty(APP_ID))
            {
                Console.WriteLine("ERROR: APP ID is not specified, cannot continue without it.");
                Console.WriteLine("Specify your Discord App's ID with the --app-id argument.");
                Console.WriteLine("Exiting...");
                return;
            }

            // Setup discord app asets url
            APP_ASSET_URL = $"{APP_URL_BASE}{APP_ID}/assets";
            
            // Create the client
            client = new DiscordRpcClient($"{APP_ID}")
            {
                Logger = new ConsoleLogger(logLevel, true)
            };

            // Subscribe to client events
            client.OnReady += OnReady;
            client.OnClose += OnClose;                                      //Called when connection to discord is lost
            client.OnError += OnError;                                      //Called when discord has a error
            client.OnConnectionEstablished += OnConnectionEstablished;      //Called when a pipe connection is made, but not ready
            client.OnConnectionFailed += OnConnectionFailed;                //Called when a pipe connection failed.
            client.OnPresenceUpdate += OnPresenceUpdate;                    //Called when the presence is updated

            // Client init
            client.Initialize();

            // Subscribe to Ctrl+C cancel keypress event adter client init
            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                isRunning = false;
                Console.WriteLine("Ctrl+C Detected, telling app to exit...");
            };

            Console.WriteLine("Press Ctrl+C to exit.");

            // Start background thread that handles main rich presence update loop
            Background.Start();
            Background.Join();

            Console.WriteLine("Exiting...");

            // Dispose of client before app exit
            client.Dispose();
		}

        static string DeleteRPAsset(string assetid)
        {
            if (string.IsNullOrEmpty(assetid))
                return "";

            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(GetDefaultHeaders());
                wc.Headers.Add("Authorization", $"{AUTH_TOKEN}");

                return System.Text.Encoding.Default.GetString(wc.UploadValues($"{APP_ASSET_URL}/{assetid}", "DELETE", new NameValueCollection()));
            }
        }

        static List<string> GetAssetsStartingWith(string nameStart)
        {
            using (var derp = new WebClient())
            {
                List<string> result = new List<string>();

                derp.Headers.Add(GetDefaultHeaders());
                derp.Headers.Add("Authorization", $"{AUTH_TOKEN}");

                string json = Encoding.UTF8.GetString(derp.DownloadData(APP_ASSET_URL));
                Asset[] assets = JsonConvert.DeserializeObject<Asset[]>(json);
                foreach (Asset a in assets)
                    if (a.name.StartsWith(nameStart))
                        result.Add(a.id);

                return result;
            }
        }

        static string GetFirstAssetId(string name)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(GetDefaultHeaders());
                wc.Headers.Add("Authorization", $"{AUTH_TOKEN}");

                string json = Encoding.UTF8.GetString(wc.DownloadData(APP_ASSET_URL));
                Asset[] result = JsonConvert.DeserializeObject<Asset[]>(json);
                foreach (Asset a in result)
                    if (a.name == name)
                        return a.id;

                return null;
            }
        }

        static WebHeaderCollection GetDefaultHeaders()
        {
            return new WebHeaderCollection
            {
                { "Cache-Control", "no-cache" },
                { "User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) discord/0.0.251 Chrome/56.0.2924.87 Discord/1.6.15 Safari/537.36" },
                { "Origin", "discord.com" },
                { "Referer", "https://discord.com/activity" }
            };
        }

        static byte[] DownloadData(string url)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                url = url.Replace("smb:", "file:///");

            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add(GetDefaultHeaders());
                return wc.DownloadData(url);
            }
        } 

        static void PrepareAssetPNGInPlace(string path)
        {
            // string imagemagickExe = "magick";
            string imagemagickExe = "convert";
            string cmd = $"\"{path}\" -resize 1024x1024 -background Transparent -gravity center -extent 1024x1024 png:\"{path}\"";
            Utils.RunProcess(imagemagickExe, cmd);
        }

        static string UploadRPAsset(string base64asset, string name)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Headers.Add("Content-Type", "application/json");
                wc.Headers.Add("Accept", "application/json");
                wc.Headers.Add(GetDefaultHeaders());
                wc.Headers.Add("Authorization", $"{AUTH_TOKEN}");

                string json = $"{{\"name\": \"{name}\", \"image\":\"{base64asset}\", \"type\":\"1\"}}";
                return System.Text.Encoding.Default.GetString(wc.UploadData(APP_ASSET_URL, "POST", Encoding.ASCII.GetBytes(json)));
            }
        }

        private static string GetNowPlayingTitle(Item itm)
        {
            string now_playing = "";
            string path = itm.file;
            if (path.EndsWith(".mkv"))
            {
                if (path.StartsWith("smb:"))
                    path = path.Substring(4);
                string json = Utils.RunProcess("mkvmerge", "-i -F json \"" + path + "\"").Item1;
                now_playing = JsonConvert.DeserializeObject<dynamic>(json)["container"]["properties"]["title"];
            }
            else
                switch (itm.type)
                {
                    case "episode":
                        now_playing = itm.showtitle == "Monogatari Series" ?
                                        itm.title :
                                        string.Format("{0} S{1:D2}E{2:D2} {3}", itm.showtitle, itm.season, itm.episode, itm.title);
                        break;

                    case "movie":
                        now_playing = itm.title;
                        break;

                    default:
                        now_playing = string.IsNullOrEmpty(itm.label) ? "" : itm.label;
                        break;
                }

            if (!string.IsNullOrEmpty(now_playing))
                if (now_playing.Length > 128)
                    now_playing = now_playing.Substring(0, 125) + "...";

            return now_playing;
        }

        public static void UpdatePresenceLoop()
        {
            while (isRunning)
            {
                // Get kodi currently playing data and update Discord Rich Presence with it
                UpdatePresence();
                // Discord min update interval is 15 seconds
                Thread.Sleep(15000);
            }
        }

        private static string KodiJsonRpcUrl(string hostname, string port, string id, string method, string[] properties)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("http://{0}:{1}/jsonrpc?request=", hostname, port);
            sb.Append("{\"jsonrpc\": \"2.0\",");
            sb.AppendFormat("\"id\": \"{0}\",", id);
            sb.AppendFormat("\"method\": \"{0}\",", method);
            sb.Append("\"params\": { \"properties\": [\"");
            sb.AppendJoin("\", \"", properties);
            sb.Append("\"],\"playerid\": 1 }}");

            return sb.ToString();
        }

        public static Result GetKodiCurrentlyPlaying()
        {
            Result result = new Result();

            string itemurl = KodiJsonRpcUrl(KODI_HOST, KODI_PORT, "GetItem", "Player.GetItem", new string[] { "title", "season", "episode", "tvshowid", "showtitle", "file", "streamdetails", "art", "mediapath" });
            string streamurl = KodiJsonRpcUrl(KODI_HOST, KODI_PORT, "GetCurrentAudioSubtitle", "Player.GetProperties", new string[] { "currentaudiostream", "currentsubtitle" });
            
            try
            {
                using (WebResponse response = WebRequest.Create(itemurl).GetResponse())
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        result = KodiJson.GetObjectFromJson(reader.ReadToEnd()).result;

                using (WebResponse response = WebRequest.Create(streamurl).GetResponse())
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        KodiJson json = KodiJson.GetObjectFromJson(reader.ReadToEnd());
                        result.currentaudiostream = json.result.currentaudiostream;
                        result.currentsubtitle = json.result.currentsubtitle;
                    }
            }
            catch (Exception ex)
            {
                string message = "Kodi Webserver Connect ERROR!" + Environment.NewLine;
                Console.WriteLine(message + ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            return result;
        }

        public static void UpdatePresence()
        {            
            Result kodiResult = GetKodiCurrentlyPlaying();
            Item mediaItem = kodiResult.item;

            string title = !String.IsNullOrEmpty(mediaItem.title)
                            ? GetNowPlayingTitle(mediaItem)
                            : "Nothing currently playing!";
            string state = kodiResult.currentaudiostream != null && kodiResult.currentsubtitle != null
                            ? $"{(String.IsNullOrEmpty(kodiResult.currentaudiostream.language) ? "Default" : kodiResult.currentaudiostream.language.ToUpper())} Dub | {(String.IsNullOrEmpty(kodiResult.currentsubtitle.language) ? "No" : kodiResult.currentsubtitle.language.ToUpper())} Sub"
                            : "";
            string posterKey = "defaultcover";

            // Setup RichPresence poster only if there is video or audio playing
            if (!(mediaItem.streamdetails.audio.Length == 0 && mediaItem.streamdetails.video.Length == 0))
            {
                string posterNew = "";
                switch (mediaItem.type)
                {
                    case "episode":
                        if (mediaItem.season == 1 || mediaItem.art.SeasonPoster == null)
                        {
                            posterNew = mediaItem.art.TVPoster;
                            posterKey = $"{mediaItem.tvshowid}-tvposter";
                        }   
                        else
                        {
                            posterNew = mediaItem.art.SeasonPoster;
                            posterKey = $"{mediaItem.tvshowid}-season-{mediaItem.season}-poster";
                        }
                        break;

                    case "unknown":
                        posterNew = mediaItem.art.Thumb;
                        posterKey = $"{mediaItem.mediapath.Split("id=")[1].ToLower()}-thumb";
                        break;

                    default:
                        posterNew = mediaItem.art.Poster;
                        posterKey = $"{mediaItem.id}-poster";
                        break;
                }
                
                // Upload the new poster asset to discord app
                if (posterOld != posterNew)
                {
                    posterOld = posterNew;
                    if (posterNew.StartsWith("image://", StringComparison.CurrentCulture))
                    {
                        string path = Uri.UnescapeDataString(posterNew.Substring(8, posterNew.Length - 9));
                        string outpath = Path.Join(ASSET_PATH, $"{posterKey}.png");
                        try
                        {
                            File.WriteAllBytes(outpath, DownloadData(path));
                            PrepareAssetPNGInPlace(outpath);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("ERROR: " + e.Message);
                            Console.WriteLine(e.StackTrace);

                            outpath = "";
                        }
                        
                        if (!String.IsNullOrEmpty(AUTH_TOKEN) && !String.IsNullOrEmpty(outpath))
                        {
                            List<string> oldPosters = GetAssetsStartingWith(posterKey);

                            if (oldPosters.Count <= 0)
                                UploadRPAsset("data:image/png;base64," + Convert.ToBase64String(File.ReadAllBytes(outpath)), posterKey);
                        }
                    }
                }
            }

            RichPresence newPresence = new RichPresence()
            {
                Details = title,
                State = state,
                Assets = new Assets()
                {
                    LargeImageKey = posterKey,
                    LargeImageText = title,
                    SmallImageKey = "kodi-logo",
                    SmallImageText = "Kodi Media Center [https://kodi.tv]"
                }
            };
            
            if (!RPEquals(oldPresence, newPresence))
            {
                client.SetPresence(newPresence);
                oldPresence = newPresence;
            }
        }

        private static bool RPEquals(RichPresence rp1, RichPresence rp2)
        {
            if (rp1 == null && rp2 == null)
                return true;

            if (rp1 == null || rp2 == null)
                return false;

            return  rp1.Details == rp2.Details &&
                    rp1.State == rp2.State &&
                    rp1.Assets.LargeImageKey == rp2.Assets.LargeImageKey &&
                    rp1.Assets.LargeImageText == rp2.Assets.LargeImageText;
        }
        
        #region Events

        #region State Events
        private static void OnReady(object sender, ReadyMessage args)
        {
            // This is called when we are all ready to start receiving and sending discord events. 
            // It will give us some basic information about discord to use in the future.
            
            // It can be a good idea to send a inital presence update on this event too, just to setup the inital game state.
            Console.WriteLine("On Ready. RPC Version: {0}", args.Version);
            Console.WriteLine("Connected to discord with user {0}", args.User.Username);
        }
        private static void OnClose(object sender, CloseMessage args)
        {
            // This is called when our client has closed. The client can no longer send or receive events after this message.
            // Connection will automatically try to re-establish and another OnReady will be called (unless it was disposed).
            Console.WriteLine("Lost Connection with client because of '{0}'", args.Reason);
        }
        private static void OnError(object sender, ErrorMessage args)
        {
            // Some error has occured from one of our messages. Could be a malformed presence for example.
            // Discord will give us one of these events and its up to us to handle it
            Console.WriteLine("Error occured within discord. ({1}) {0}", args.Message, args.Code);
        }
        #endregion

        #region Pipe Connection Events
        private static void OnConnectionEstablished(object sender, ConnectionEstablishedMessage args)
        {
            // This is called when a pipe connection is established. The connection is not ready yet, but we have at least found a valid pipe.
            Console.WriteLine("Pipe Connection Established. Valid on pipe #{0}", args.ConnectedPipe);
        }
        private static void OnConnectionFailed(object sender, ConnectionFailedMessage args)
        {
            // This is called when the client fails to establish a connection to discord. 
            // It can be assumed that Discord is unavailable on the supplied pipe.
            Console.WriteLine("Pipe Connection Failed. Could not connect to pipe #{0}", args.FailedPipe);
            isRunning = false;
        }
        #endregion

        private static void OnPresenceUpdate(object sender, PresenceMessage args)
        {
            // This is called when the Rich Presence has been updated in the discord client.
            // Use this to keep track of the rich presence and validate that it has been sent correctly.
            if (args.Presence != null)
            {
                Console.WriteLine();
                Console.WriteLine("[RICH PRESENCE UPDATED]");
                Console.WriteLine($"TITLE:\t{args.Presence.Details}");
                Console.WriteLine($"STATE:\t{args.Presence.State}");
                Console.WriteLine($"IMAGE:\t{args.Presence.Assets.LargeImageKey}");
            }
        }

        #endregion
    }
}
