﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KodiDiscordRPCThingy
{
    public class KodiJson
    {
        public string id { get; set; }
        public string jsonrpc { get; set; }
        public Result result { get; set; }

        public static KodiJson GetObjectFromJson(string jsonString) => JsonConvert.DeserializeObject<KodiJson>(jsonString);
        public override bool Equals(object obj)
        {
            KodiJson kj = obj as KodiJson;
            bool derp = false;

            if (this != null && kj != null)
            {
                derp =
                    id == kj.id &&
                    jsonrpc == kj.jsonrpc &&
                    result.Equals(kj.result)
                    ;
            }
            else if (this == null && obj == null)
                derp = true;
            else
                derp = false;

            return derp;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class Result
    {
        public Item item { get; set; }
        public AudioStream currentaudiostream { get; set;}
        public ItemStream currentsubtitle { get; set; }

        public override bool Equals(object obj)
        {
            return item.Equals(((Result)obj).item);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class Item
    {
        public int episode { get; set; }
        public string file { get; set; }
        public int id { get; set; }
        public string label { get; set; }
        public int season { get; set; }
        public int tvshowid { get; set; }
        public string showtitle { get; set; }
        public ItemStreamDetails streamdetails { get; set; }
        public string title { get; set; }
        public string type { get; set; }

        [JsonProperty("mediapath")]
        public string mediapath { get; set; }
        public Art art { get; set; }

        public override bool Equals(object obj)
        {
            Item itm = obj as Item;

            bool derp =
                episode == itm.episode &&
                file == itm.file &&
                id == itm.id &&
                label == itm.label &&
                season == itm.season &&
                tvshowid == itm.tvshowid &&
                showtitle == itm.showtitle &&
                title == itm.title &&
                type == itm.type
                ;

            Console.WriteLine($"EPISODE:\t\t{episode} == {itm.episode}");
            Console.WriteLine($"FILE:\t{episode = itm.episode}\t{file} == {itm.file}");
            Console.WriteLine($"ID:\t\t{id} == {itm.id}");
            Console.WriteLine($"LABEL:\t\t{label} == {itm.label}");
            Console.WriteLine($"SEASON:\t\t{season} == {itm.season}");
            Console.WriteLine($"TVSHOWID: \t\t{tvshowid} == {itm.tvshowid}");
            Console.WriteLine($"SHOWTITLE:\t\t{showtitle} == {itm.showtitle}");
            Console.WriteLine($"TITLE:\t\t{title} == {itm.title}");
            Console.WriteLine($"TYPE:\t\t{type} == {itm.type}");
            Console.WriteLine($"RESULT = {derp.ToString()}");

            return derp;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class ItemStreamDetails
    {
        public AudioStream[] audio { get; set; }
        public ItemStream[] subtitle { get; set; }
        public VideoStream[] video { get; set; }
    }

    public class ItemStream
    {
        public string index { get; set; }
        public bool isdefault { get; set; }
        public bool isforced { get; set; }
        public bool isimpaired { get; set; }
        public string language { get; set; }
    }

    public class AudioStream : ItemStream
    {
        public int channels { get; set; }
        public string codec { get; set; }
    }

    public class VideoStream : ItemStream
    {
        public double aspect { get; set; }
        public string codec { get; set; }
        public int duration { get; set; }
        public int height { get; set; }
        public string stereomode { get; set; }
        public int width { get; set; }
    }

    public class Art
    {
        [JsonProperty("fanart")]
        public string Fanart { get; set; }

        [JsonProperty("poster")]
        public string Poster { get; set; }

        [JsonProperty("thumb")]
        public string Thumb { get; set; }

        [JsonProperty("season.poster")]
        public string SeasonPoster { get; set; }

        [JsonProperty("tvshow.banner")]
        public string TVBanner { get; set; }

        [JsonProperty("tvshow.fanart")]
        public string TVFanart { get; set; }

        [JsonProperty("tvshow.poster")]
        public string TVPoster { get; set; }
    }
}
