﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KodiDiscordRPCThingy
{
    class Utils
    {
        public enum DATA_TYPE
        {
            STRING,
            BINARY
        }

        // taken from http://www.dotnetperls.com/redirectstandardoutput
        /// <summary>
        /// Method to run a command line process with arguments, redirecting stdout and returning it as a string
        /// </summary>
        /// <param name="exeName">Name of the Executable program</param>
        /// <param name="args">Command line paguments to pass to the program</param>
        /// <returns>stdout from program</returns>
        public static Tuple<string, byte[]> RunProcess(string exeName, string args, DATA_TYPE dt = DATA_TYPE.STRING)
        {
            ProcessStartInfo start = new ProcessStartInfo
            {
                FileName = exeName,                                   // process file name
                Arguments = args,                                     // process arguments
                UseShellExecute = false,                              // don't use OS shell to run process
                RedirectStandardOutput = true,                        // redirect the stdout from process back to program
                CreateNoWindow = false                                // don't create a visible window for process
            };            // Instantiate new ProcessStartInfo for process

            switch(dt)
            {
                case DATA_TYPE.BINARY:
                    using (Process process = Process.Start(start))
                    {
                        using (BinaryReader derp = new BinaryReader(process.StandardOutput.BaseStream))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                derp.BaseStream.CopyTo(ms);
                                Console.ReadKey();
                                return new Tuple<string, byte[]>(null, ms.ToArray());
                            }
                        }
                    }

                default:
                    using (Process process = Process.Start(start))              // Start the process
                    {
                        using (StreamReader reader = process.StandardOutput)    // Create reader to read process output
                        {
                            return new Tuple<string, byte[]>(reader.ReadToEnd(), null);                 // read all output and return
                        }
                    }
            }
        }
    }
}
