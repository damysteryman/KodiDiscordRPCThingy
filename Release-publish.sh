#!/bin/bash

targets=("linux-x64:" "win-x64:" "linux-x64:portable" "win-x64:portable")
ver=1.0.0
rev="$(( $(git rev-list --count HEAD)))-$(git rev-list HEAD | head -n 1 -c 8)"
fullver="${ver}.${rev}"

echo "UPDATING TO VER: $ver.$rev"
PROJ="KodiDiscordRPCThingy"
sed -i "s/Version>.*</Version>${fullver}</g" "./Directory.Build.props"
NET_VER=$(cat ${PROJ}.csproj | grep TargetFramework | sed "s;.*<TargetFramework>\(.*\)</TargetFramework>;\1;g")

for t in ${targets[@]}
do
	target="$(echo $t | cut -d':' -f1 )"
	mode="$(echo $t | cut -d':' -f2 )"
	if [ ! -z "${mode}" ]; then
		mode="-${mode}"
	fi
	echo ""
	echo "PUBLISHING: ${target} ${mode}"
	rm -r "./bin/Release"
	rm -r "./obj/Release/${NET_VER}/${t}"
	if [ "${mode}" == "-portable" ]; then
		rm "./publish/KodiDiscordRPCThingy-v${fullver}-${target}${mode}"
		dotnet publish -r "${target}" -c Release --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true 
	else
		rm "./publish/KodiDiscordRPCThingy-v${fullver}-${target}"
		dotnet publish -r "${target}" -c Release --self-contained false -p:PublishSingleFile=true
	fi
	rm "./bin/Release/${NET_VER}/${target}/publish/KodiDiscordRPCThingy.pdb"
	for file in $(find "./bin/Release/${NET_VER}/${target}/publish" | awk "NR>1")
	do
		mv -v "${file}" "publish/`echo "$file" | awk -F'/' '{print $NF}' | sed "s/${PROJ}/KodiDiscordRPCThingy-v${fullver}-${target}${mode}/g"`"
	done
done